package com.len.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import com.len.validator.group.AddGroup;
import com.len.validator.group.UpdateGroup;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Table(name = "SYS_ROLE")
@Data
@ToString
@EqualsAndHashCode
public class SysRole {
    @Id
    @Column(name = "ID")
    private String id;

    @NotEmpty(message = "角色名称不能为空", groups = {AddGroup.class, UpdateGroup.class})
    @Column(name = "ROLE_NAME")
    private String roleName;

    private String remark;

    @Column(name = "CREATE_BY")
    private String createBy;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "UPDATE_BY")
    private String updateBy;

    @Column(name = "UPDATE_DATE")
    private Date updateDate;
}