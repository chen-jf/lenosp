package com.len.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import com.len.validator.group.AddGroup;
import com.len.validator.group.UpdateGroup;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Table(name = "SYS_MENU")
@Data
@ToString
@EqualsAndHashCode
public class SysMenu {
    @Id
    @Column(name = "ID")
    private String id;

    @NotEmpty(message = "菜单名称不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String name;

    @Column(name = "P_ID")
    private String pId;

    private String url;

    /**
     * 排序字段
     */
    @Length(min = 1,max = 4, message = "序号长度不对")
    @Column(name = "ORDER_NUM")
    private Integer orderNum;

    /**
     * 图标
     */
    private String icon;

    @Column(name = "CREATE_BY")
    private String createBy;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "UPDATE_BY")
    private String updateBy;

    @Column(name = "UPDATE_DATE")
    private Date updateDate;

    /**
     * 权限
     */
    private String permission;

    /**
     * 1栏目2菜单
     */
    @Column(name = "MENU_TYPE")
    private Byte menuType;

    private int num;

    private List<SysRole> roleList;

    private static final long serialVersionUID = 1L;

    private List<SysMenu> children=new ArrayList<SysMenu>();

    public void addChild(SysMenu sysMenu){
        children.add(sysMenu);
    }
}