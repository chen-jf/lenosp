package com.len.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import com.len.validator.group.AddGroup;
import com.len.validator.group.UpdateGroup;

import lombok.Data;
import lombok.ToString;

@Table(name = "SYS_USER")
@Data
@ToString
public class SysUser {
    @Id
    @Column(name = "ID")
    private String id;

    @NotEmpty(message = "用户名不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String username;

    @NotEmpty(message = "密码不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String password;

    private Integer age;

    private String email;

    private String photo;

    @Column(name = "REAL_NAME")
    private String realName;

    @Column(name = "CREATE_BY")
    private String createBy;

    @Column(name = "UPDATE_BY")
    private String updateBy;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "UPDATE_DATE")
    private Date updateDate;

    /**
     * 0可用1封禁
     */
    @Column(name = "DEL_FLAG")
    private Byte delFlag;
}