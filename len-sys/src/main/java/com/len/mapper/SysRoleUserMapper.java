package com.len.mapper;

import java.util.List;

import com.len.base.BaseMapper;
import com.len.entity.SysRoleUser;

public interface SysRoleUserMapper extends BaseMapper<SysRoleUser,String> {

    List<SysRoleUser> selectByCondition(SysRoleUser sysRoleUser);

    int selectCountByCondition(SysRoleUser sysRoleUser);
}