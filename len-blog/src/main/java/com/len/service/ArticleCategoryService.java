package com.len.service;

import java.util.List;

import com.len.base.BaseService;
import com.len.entity.ArticleCategory;

/**
 * @author zhuxiaomeng
 * @date 2018/10/11.
 * @email 154040976@qq.com
 */
public interface ArticleCategoryService extends BaseService<ArticleCategory, String> {

    void delByIds(List<String> ids);
}
